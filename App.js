import App from './src/App'
import { configure } from 'mobx';

configure({
    enforceActions: true
});

export default App
