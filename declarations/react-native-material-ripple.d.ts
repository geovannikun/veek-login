declare module 'react-native-material-ripple' {
    import React from "react";
    import { TouchableWithoutFeedbackProps } from "react-native";
    
    export interface RippleProps extends TouchableWithoutFeedbackProps {
        rippleColor?: String,
        rippleOpacity?: Number,
        rippleDuration?: Number,
        rippleSize?: Number,
        rippleContainerBorderRadius?: Number,
        rippleCentered?: Boolean,
        rippleSequential?: Boolean,
        rippleFades?: Boolean,
        onRippleAnimation?: Function,
    }
    
    export default class Ripple extends React.Component<RippleProps> {}
}