import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import { Constants } from 'expo';
import GlobalStyle from '../styles/GlobalStyle';

export interface HeaderProps {
    title: string;
}

export default class Header extends React.Component<HeaderProps> {
    render(){
        const { title } = this.props;
        return (
            <View style={Styles.container}>
                <Ionicons name="md-arrow-back" size={42} style={Styles.backBtn}/>
                <Text style={Styles.title}>{title}</Text>
            </View>
        );
    }
}

const Styles = {
    container: {
        paddingTop: Constants.statusBarHeight,
        flexDirection: 'column',
    } as StyleProp<ViewStyle>,
    backBtn: {
        margin: 21,
        color: GlobalStyle.fontColor.primary,
    } as StyleProp<TextStyle>,
    title: {
        color: GlobalStyle.fontColor.primary,
        fontSize: 38,
        lineHeight: 45,
        paddingLeft: 28,
        fontFamily: 'montserrat'
    } as StyleProp<TextStyle>
};