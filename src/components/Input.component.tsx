import React from 'react';
import { View, Text, TextInput } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import GlobalStyle from '../styles/GlobalStyle';
import Ripple from 'react-native-material-ripple';
import { TextInputMask } from 'react-native-masked-text'
import { observable, action, computed } from "mobx";
import { observer } from 'mobx-react';

type event = {
    nativeEvent: {
        text: string;
        contentSize: { width: number; height: number };
        target: number;
        eventCount: number;
    };
}

export interface InputProps {
    label: string;
    onChange: (
        event: event
    ) => void;
    placeholder: string;
    password?: boolean;
    style?: StyleProp<ViewStyle>,
    type?: string,
}

@observer
export default class Input extends React.Component<InputProps> {
    @observable visible = false;
    @observable value = '';
    @action showHandler = () => {
        this.visible = !this.visible;
    }
    @action onChange = (event: event) => {
        this.value = event.nativeEvent.text;
        this.props.onChange(event);
    }
    @computed get visibleClass() {
        return this.visible ? 'visibility' : 'visibility-off'
    }
    @computed get visibleColor() {
        return this.visible ? GlobalStyle.fontColor.link : '#DDD'
    }
    render(){
        const { label, placeholder, password = false, type } = this.props;
        return (
            <View style={[Styles.container, this.props.style]}>
                <Text style={Styles.label}>{label}</Text>
                <CustomInput
                    type={type}
                    value={this.value}
                    placeholder={placeholder}
                    style={Styles.input}
                    selectionColor={GlobalStyle.fontColor.primary}
                    underlineColorAndroid="transparent"
                    onChange={this.onChange}
                    secureTextEntry={password && !this.visible}/>
                {password && (
                    <Ripple onPress={this.showHandler} style={Styles.showBtn} rippleContainerBorderRadius={36} rippleCentered={true}>
                        <MaterialIcons name={this.visibleClass} size={36} color={this.visibleColor}/>
                    </Ripple>
                )}
            </View>
        );
    }
}

const CustomInput = (props: any) => props.type ? (<TextInputMask {...props}/>) : (<TextInput {...props}/>);

const Styles = {
    container: {
        flexDirection: 'column',
    } as StyleProp<ViewStyle>,
    showBtn: {
        position: 'absolute',
        bottom: 25,
        right: 0,
    } as StyleProp<TextStyle>,
    input: {
        height: 90,
        fontSize: 38,
        color: GlobalStyle.fontColor.primary,
        fontFamily: 'montserrat-extra-light',
        borderBottomWidth: 1,
        borderBottomColor: '#DCDCDC'
    } as StyleProp<TextStyle>,
    label: {
        color: GlobalStyle.fontColor.primary,
        fontSize: 18,
        lineHeight: 24,
        fontFamily: 'montserrat',
    } as StyleProp<TextStyle>
};