import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { LoginScreen } from './screens';
import { Font } from 'expo';
import { observer } from 'mobx-react';
import { observable, action, runInAction } from 'mobx';

@observer
export default class App extends React.Component {
  @observable fontsLoaded = false;
  @action async componentDidMount() {
    await Font.loadAsync({
      'montserrat': require('./assets/fonts/Montserrat-Medium.ttf'),
      'montserrat-semi-bold': require('./assets/fonts/Montserrat-SemiBold.ttf'),
      'montserrat-light': require('./assets/fonts/Montserrat-Light.ttf'),
      'montserrat-extra-light': require('./assets/fonts/Montserrat-ExtraLight.ttf'),
    });
    runInAction(() => {
      this.fontsLoaded = true;
    })
  }

  render(){
    return this.fontsLoaded && (<AppNavigator/>)
  }
}

const AppNavigator = createStackNavigator({
  Login: {
    screen: LoginScreen
  },
},{
  headerMode: 'none',
});
