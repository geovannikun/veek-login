export default {
    fontColor: {
        primary: '#233B78',
        secondary: '#757575',
        link: '#09BCD3',
    },
    button: {
        primary: '#E92087'
    }
}