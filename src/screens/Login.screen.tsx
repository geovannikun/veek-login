
import React from 'react';
import { Text, View } from 'react-native';
import { Header, Input } from '../components';
import { StyleProp, ViewStyle, TextStyle } from 'react-native';
import GlobalStyle from '../styles/GlobalStyle';
import Ripple from 'react-native-material-ripple';
import { KeyboardAvoidingView } from 'react-native';
import { Dimensions } from 'react-native';
import { Alert } from 'react-native';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

@observer
export default class LoginScreen extends React.Component {
  @observable number = '';
  @observable pass = '';
  loginHandler = () => {
    Alert.alert(
      'Login',
      `Numero: ${this.number}\nSenha: ${this.pass}`,
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      { cancelable: true }
    )
  }
  @action numberHandler = (e: any) => {
    this.number = e.nativeEvent.text;
  }
  @action passHandler = (e: any) => {
    this.pass = e.nativeEvent.text;
  }
  render() {
    return (
      <View style={Styles.container}>
        <KeyboardAvoidingView behavior="position" style={{flex: 1, height: Dimensions.get('window').height}}>
          <Header title="Login"/>
          <View style={Styles.content}>
            <Text style={Styles.description}>Acesse com seu número Veek senha.</Text>
            <Input label="Seu número Veek" placeholder="DDD + número Veek" onChange={this.numberHandler} style={Styles.input} type="cel-phone"/>
            <Input label="Senha" placeholder="Sua senha" onChange={this.passHandler} style={Styles.input} password={true}/>
          </View>
          <Ripple onPress={this.loginHandler} style={Styles.loginBtn}>
            <Text style={Styles.loginBtnText}>ENTRAR</Text>
          </Ripple>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const Styles = {
  input: {
    marginTop: 30,
  } as StyleProp<ViewStyle>,
  description: {
    color: GlobalStyle.fontColor.secondary,
    fontSize: 26,
    fontFamily: 'montserrat-light',
  } as StyleProp<TextStyle>,
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  } as StyleProp<ViewStyle>,
  content: {
    flexDirection: 'column',
    padding: 27,
  } as StyleProp<ViewStyle>,
  loginBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: GlobalStyle.button.primary,
    margin: 27,
    borderRadius: 2,
    height: 89,
  } as StyleProp<ViewStyle>,
  loginBtnText: {
    fontFamily: 'montserrat-semi-bold',
    color: 'white',
    fontSize: 25,
  } as StyleProp<TextStyle>,
}